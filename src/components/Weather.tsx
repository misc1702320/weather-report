import React, { useEffect, useState } from 'react';

type WeatherReport = {
    daily: {
        [key: string]: any[]
    }
}

interface Props {
    lat: number;
    long: number;

    variables: string[];
}

const Weather: React.FC<Props> = props => {
    const [weather, setWeather] = useState<WeatherReport>()

    useEffect(() => {
        fetch(`https://api.open-meteo.com/v1/forecast?latitude=${props.lat}&longitude=${props.long}&daily=${props.variables.join(',')}&timezone=Europe/Moscow&past_days=0`, { method: 'GET' }).then(resp => {
            if (resp.ok) {
                resp.json().then((content) => {
                    setWeather(content)
                })
            }
        })
    }, [props.variables, props.lat, props.long])


    return <table style={{ width: '100%' }}>
        <thead>
            <tr>
                <td >date</td>
                {props.variables.map(variable => <td key={variable}>{variable}</td>)}
            </tr>
        </thead>

        <tbody>
            {weather && weather.daily.time.map((time, index) => <tr>
                <td key={time}>
                    {time}
                </td>
                {props.variables.map(variable =>
                    <td key={variable}>
                        {weather.daily[variable] ? weather.daily[variable][index] : "Loading..."}
                    </td>
                )}
            </tr>)}
        </tbody>
    </table>
}


export default React.memo(Weather);
